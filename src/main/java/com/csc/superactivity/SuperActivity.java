package com.csc.superactivity;

import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;

import com.appcelerator.aps.APSAnalytics;
import com.appcelerator.aps.APSCloudException;
import com.appcelerator.aps.APSCloudPush;
import com.appcelerator.aps.APSPushNotifications;
import com.appcelerator.aps.APSResponse;
import com.appcelerator.aps.APSResponseHandler;
import com.appcelerator.aps.APSRetrieveDeviceTokenHandler;
import com.appcelerator.aps.APSServiceManager;

public class SuperActivity extends Activity {
	
	public static final APSAnalytics analytics = APSAnalytics.getInstance();
	public static boolean isInitialized;
	private static final String TAG = SuperActivity.class.getSimpleName();
	
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		
		APSServiceManager.getInstance().enable(getApplicationContext(), "KCLxlbC2ZcF95IsiOMFI/ta0kcD9h48xx2JvROMvuQHbuTVSL3fw0zhVP+UkB8EwYvFuTxBRzrntUBxNbmexMEdoh+sFOjsOBAE4L3JDkGA/RoV4aakCOcSgFsrxE2OSgwnhNwIJyLWskpC8PsDlg4bN2KPNEHOgTAGENNYgi6a6QA/6JcGOeLjSGBuRIXDai+a3negx7ZodnKhmXy918xiz+dzJcDNAu8T11/DiK5vQf1gi6PMTCmbywS13tJVd49NDaxoNmC/jUu+SXcg7qteJjFpx/SoZHDQiAaL1g4HKL57sd4MvJjCwBbIYNh+Eb64xO+pfdZjbACICgcwycw==");
		Log.d(TAG, "APSServiceManager enabled.");
		
		// Set Session Timeout Here (Default is 30 seconds)
		analytics.setSessionTimeout(30, TimeUnit.SECONDS);
		Log.d(TAG, "setSessionTimeout");
		
		// Setup Push Notifications
		setupPush();
		
		isInitialized = true;
	}
	@Override
	protected void onResume()
	{
		super.onResume();
		if (isInitialized) {
			analytics.sendAppForegroundEvent();
			Log.d(TAG, "sendAppForegroundEvent");
		}
	}

	@Override
	protected void onPause()
	{
		super.onPause();
		if (isInitialized) {
			analytics.sendAppBackgroundEvent();
			Log.d(TAG, "sendAppBackgroundEvent");
		}
	}
	
	private void setupPush() {
		APSCloudPush.getInstance().retrieveDeviceToken(new APSRetrieveDeviceTokenHandler() {
		    @Override
		    public void onError(String message) {
		        Log.e("APSCloudPush", "Could not retrieve device token: " + message);
		    }

		    @Override
		    public void onSuccess(String deviceToken) {
		        HashMap<String, Object> data = new HashMap<String, Object>();
		        data.put("type", "android");
		        data.put("channel", "general");
		        data.put("device_token", deviceToken);
		        try {
		            APSPushNotifications.subscribeToken(data, new APSResponseHandler() {

		                @Override
		                public void onResponse(final APSResponse e) {
		                    if (e.getSuccess()) {
		                        Log.i("APSPushNotifications", "Subscribed!");
		                    } else {
		                        Log.e("APSPushNotifications", "ERROR: " + e.getErrorMessage());
		                    }
		                }

		                @Override
		                public void onException(final APSCloudException e) {
		                    Log.e("APSPushNotifications", "Exception throw: " + e.toString());
		                }
		            });
		        } catch (APSCloudException e) {
		            Log.e("APSPushNotifications", "Exception thrown: " + e.toString());
		        }
		    }
		});
	}

}

/**
 * Appcelerator Platform SDK
 * Copyright (c) 2014 by Appcelerator, Inc. All Rights Reserved.
 * Proprietary and Confidential - This source code is not for redistribution
 */

package com.csc.superactivity;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import com.example.apsanalyticstest.R;

public class MainActivity extends SuperActivity
{

	private static final String TAG = MainActivity.class.getSimpleName();

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);

		setContentView(R.layout.mainlayout);

		Button geoButton = (Button) findViewById(R.id.geoButton);
		geoButton.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				if (SuperActivity.isInitialized) {
					SuperActivity.analytics.sendAppGeoEvent(new Location("myProvider"));
					Log.d(TAG, "sendAppGeoEvent");
				}
			}
		});

		Button navButton = (Button) findViewById(R.id.navButton);
		navButton.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				JSONObject data = new JSONObject();
				try {	
					// 'eventName' and 'to' should not show up in analytics since it conflicts with default properties.
					data.put("eventName", "badEvent");
					data.put("to", "badTo");
					data.put("page", "one");
				} catch (JSONException e) {
					e.printStackTrace();
				}
				if (SuperActivity.isInitialized) {
					SuperActivity.analytics.sendAppNavEvent("from", "to", "navEvent", null);
					Log.d(TAG, "sendAppNavEvent");
				}
			}
		});

		Button customButton = (Button) findViewById(R.id.customEvent);
		customButton.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				JSONObject data = new JSONObject();
				try {
					// 'eventName' should not show up since it conflicts with default properties
					data.put("foo", "bar");
					data.put("eventName", "badEvent");
					if (SuperActivity.isInitialized) {
						SuperActivity.analytics.sendAppFeatureEvent("myfeature", data);
						Log.d(TAG, "sendAppFeatureEvent: myfeature");
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		});
		
		Button newActivityButton = (Button) findViewById(R.id.otherActivity);
		newActivityButton.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				// Open the Other Activity
				Intent intent = new Intent(MainActivity.this, OtherActivity.class);
				startActivity(intent);
				
				if (SuperActivity.isInitialized) {
					SuperActivity.analytics.sendAppFeatureEvent("otherActivity", null);
					Log.d(TAG, "sendAppFeatureEvent: otherActivity");
				}
			}
		});
	}

	@Override
	protected void onResume()
	{
		super.onResume();
	}

	@Override
	protected void onPause()
	{
		super.onPause();
	}
}

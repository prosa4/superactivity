package com.csc.superactivity;

import com.example.apsanalyticstest.R;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

public class OtherActivity extends SuperActivity {
	
	private static final String TAG = OtherActivity.class.getSimpleName();
	
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.otherlayout);
		
		Button otherEventButton = (Button) findViewById(R.id.otherEventButton);
		otherEventButton.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				// Send Other Event
				if (isInitialized) {
					analytics.sendAppFeatureEvent("otherEvent", null);
					Log.d(TAG, "sendAppFeatureEvent: otherEvent");
				}
				
				Toast.makeText(getApplicationContext(), "Other Event", Toast.LENGTH_SHORT).show();
			}
		});
	}
	@Override
	protected void onResume()
	{
		super.onResume();
	}

	@Override
	protected void onPause()
	{
		super.onPause();
	}
}
